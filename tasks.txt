SKILLS
-----
- Using Tokens

MODULES
-----
- x

HOOKS
-----
- hook_token_info()
- hook_tokens()

STEPS
-----	
- Read about the Tokens module
- Create a module with a settings form
- Add a textarea to the textform
- Make it possible to add Tokens to the textarea
- Define a Block that outputs the value of the textarea with the Tokens replaced
- Add a dropdown with Nodes to your settings form
- Make it possible to use Tokens from the chosen Node in the textarea
- Add two input fields to the settings form
- Use hook_token_info() to add these settings to the Token tree and use them in the textarea

